# Sharepoint App Connector for iOS #

Created a Sharepoint library that extends that capability of the office 365 sdk for iOS.


##  Installing dependencies ##

1. Install Sharepoint Lists service from the Office 365 SDK for iOS
     https://github.com/OfficeDev/Office-365-SDK-for-iOS
2. Copy the following files into your project. These are the files that you will used to make transactions with the Sharepoint rest api.
     * AuthenticationManager.h
     * AuthenticationManager.m
     * ListClient+ListItemClient.h
     * ListClient+ListItemClient.m
     * Office365ClientFetcher+SharepointClient.h 
     * Office365ClientFetcher+SharepointClient.m
     * Office365ClientFetcher.h
     * Office365ClientFetcher.m 

##  Authenticate the application with Sharepoint Client ##

In order to connect to the sharepoint api, first we must authenticate the user with our registered application URI's. In order to do that, we need to follow these steps:

* Set your endpoint uri inside AuthenticationManager.m
```
#!objective-c

// You will set your application's clientId and redirect URI. You get
// these when you register your application in Azure AD.
static NSString * const REDIRECT_URL_STRING = @"http://www.p3ople4u.com";
static NSString * const CLIENT_ID           = @"35142199-c83c-4cbb-8125-a5baaab4c194";
static NSString * const AUTHORITY           = @"https://login.microsoftonline.com/common";
```

* You also need to set your Sharepoint uri inside Office365ClientFetcher+SharepointClient.m

```
#!objective-c

static NSString * const SHAREPOINT_URI  = @"https://p3ople4me.sharepoint.com";
```

##  Accessing the Sharepoint Client ##

In order to call the apis we must first get a sharepoint client via the office365ClientFetcher.


```
#!objective-c

Office365ClientFetcher *baseController = [[Office365ClientFetcher alloc] init];
//fetchListClient method returns Sharepoint list client instance. This instance will be used to call the api calls.

ListClient *sharepointListClient = nil;
[baseController fetchListClient:^(ListClient *listClient) {
     //assign list client here
     sharepointListClient = listClient;
}];
```

##  Making api calls using the Sharepoint client ##
Once the sharepoint client has been created, you can now call the apis in our library. All calls are of NSURSessionTask type methods.

* ### Get Items ###

```
#!objective-c

NSURLSessionTask* task = [sharepointListClient getListItems:@"Your list name" callback:^(NSMutableArray *listItems, NSError *error) {
                //Will return an array of item in the list
}];
[task resume];
```

* ### Get Specific Item ###

```
#!objective-c

NSURLSessionTask* task = [sharepointListClient fetchItemWithId:@"1" andListName:@"My List Name" callback:^(ListItem *item, NSError *error) {
        //returns ListItem object
    }];
[task resume];
```

* ### Create Item ###

```
#!objective-c

//Format is @"FieldnameOfYourAppOrList" : @"ValueOfTheField"
NSDictionary *dataDict = @{@"Title": @"Your Title here",
                           @"FirstName": @"Juan",
                           @"Email": @"j@p34.com",
                           @"Company": @"Company",
                           @"JobTitle": @"Developer"
                            };

NSURLSessionTask *task = [sharepointListClient createItemWithData:dataDict
                                                WithListName:@"My List Name" callback:^(ListItem *item, NSError *error) {
}];
[task resume];
```

* ### Updating an Item ###

```
#!objective-c

//Format is @"FieldnameOfYourAppOrList" : @"ValueOfTheField"
NSDictionary *dataDict = @{@"Title": @"Your Title here",
                           @"FirstName": @"Juan",
                           @"Email": @"j@p34.com",
                           @"Company": @"Company",
                           @"JobTitle": @"Developer"
                            };

NSURLSessionTask* task = [sharepointListClient updateItemWithId:@"1" withData:dataDict andListName:@"My List Name" callback:(void (^)(NSError *error) {
        //returns error object
    }];
[task resume];

```

* ### Deleting an Item ###

```
#!objective-c

NSURLSessionTask* task = [sharepointListClient deleteItemWithId:@"1" andListName:@"My List Name" callback:(void (^)(NSError *error) {
        //returns error object
    }];
[task resume];

```