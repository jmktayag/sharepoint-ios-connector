//
//  main.m
//  Office356Template
//
//  Created by P3OPLE 4U on 5/20/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
