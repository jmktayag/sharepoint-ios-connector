//
//  ViewController.m
//  Office356Template
//
//  Created by P3OPLE 4U on 5/20/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "ViewController.h"
#import "Office365ClientFetcher.h"
#import "MSDiscoveryServiceInfoCollectionFetcher.h"
#import "Office365ClientFetcher+SharepointClient.h"

@interface ViewController ()

@property (strong, nonatomic) Office365ClientFetcher *baseController;
@property (strong, nonatomic) NSMutableDictionary *serviceEndpointLookup;

@end

@implementation ViewController

- (Office365ClientFetcher *)baseController
{
    if (!_baseController) {
        _baseController = [[Office365ClientFetcher alloc] init];
    }
    
    return _baseController;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //[self connectToOffice365];
    
    [self connecttoSharepointListClient];
}

#pragma mark - Helper Methods
- (void)connectToOffice365
{
    // Connect to the service by discovering the service endpoints and authorizing
    // the application to access the user's email. This will store the user's
    // service URLs in a property list to be accessed when calls are made to the
    // service. This results in two calls: one to authenticate, and one to get the
    // URLs. ADAL will cache the access and refresh tokens so you won't need to
    // provide credentials unless you sign out.
    
    // Get the discovery client. First time this is ran you will be prompted
    // to provide your credentials which will authenticate you with the service.
    // The application will get an access token in the response.
    [self.baseController fetchDiscoveryClient:^(MSDiscoveryClient *discoveryClient) {
        MSDiscoveryServiceInfoCollectionFetcher *servicesInfoFetcher = [discoveryClient getservices];
        
        // Call the Discovery Service and get back an array of service endpoint information
        NSURLSessionTask *servicesTask = [servicesInfoFetcher readWithCallback:^(NSArray *serviceEndpoints, MSODataException *error) {
            if (serviceEndpoints) {
                
                // Here is where we cache the service URLs returned by the Discovery Service. You may not
                // need to call the Discovery Service again until either this cache is removed, or you
                // get an error that indicates that the endpoint is no longer valid.
                self.serviceEndpointLookup = [[NSMutableDictionary alloc] init];
                
                for(MSDiscoveryServiceInfo *serviceEndpoint in serviceEndpoints) {
                    self.serviceEndpointLookup[serviceEndpoint.capability] = serviceEndpoint.serviceEndpointUri;
                }
                
                // Keep track of the service endpoints in the user defaults
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setObject:self.serviceEndpointLookup
                                 forKey:@"O365ServiceEndpoints"];
                                
                [userDefaults synchronize];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error in the authentication: %@", error);
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                    message:@"Authentication failed. This may be because the Internet connection is offline  or perhaps the credentials are incorrect. Check the log for errors and try again."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                });
            }
        }];
        
        [servicesTask resume];
    }];
}

-(void)connecttoSharepointListClient {
    [self.baseController fetchListClient:^(ListClient *listClient) {
        NSLog(@"**listClient: %@", listClient);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
