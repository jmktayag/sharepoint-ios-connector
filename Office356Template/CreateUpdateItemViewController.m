//
//  CreateUpdateItemViewController.m
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "CreateUpdateItemViewController.h"

#define LIST_NAME @"MckeinContacts"

@interface CreateUpdateItemViewController ()

@end

@implementation CreateUpdateItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *dictData = self.listItem.getRawResults;
    
    self.lastNameTextField.text = dictData[@"Title"];
    self.firstnameTextField.text = dictData[@"FirstName"];
    self.emailTextField.text = dictData[@"Email"];
    self.companyTextField.text = dictData[@"Company"];
    self.jobtitleTextField.text = dictData[@"JobTitle"];

    if (self.action == Create) {
        self.title = @"Create Item";
        self.navigationItem.rightBarButtonItem.title = @"Create";
    } else if(self.action == Update){
        self.title = @"Update Item";
        self.navigationItem.rightBarButtonItem.title = @"Update";
    } else {
        self.title = @"View Item";
        self.navigationItem.rightBarButtonItem.title = @"Edit";
        self.lastNameTextField.enabled = NO;
        self.firstnameTextField.enabled = NO;
        self.emailTextField.enabled = NO;
        self.companyTextField.enabled = NO;
        self.jobtitleTextField.enabled = NO;
    }
}

- (IBAction)createItem:(id)sender {
    //Create List Item
    
    switch (_action) {
        case Create: {
            NSURLSessionTask *task = [self.listClient createItemWithData:@{@"Title": self.lastNameTextField.text,
                                                                           @"FirstName": self.firstnameTextField.text,
                                                                           @"Email": self.emailTextField.text,
                                                                           @"Company": self.companyTextField.text,
                                                                           @"JobTitle": self.jobtitleTextField.text
                                                                           }
                                                            WithListName:LIST_NAME callback:^(ListItem *item, NSError *error) {
                                                                
                                                                if (item) {
                                                                    [self performSegueWithIdentifier:@"UpdateList" sender: nil];
                                                                }
                
                                                            }];
            [task resume];
        }
            
            break;
        case Update: {
            //Update call
            NSURLSessionTask *task = [self.listClient updateItem:self.listItem
                                                        withData:@{@"Title": self.lastNameTextField.text,
                                                                   @"FirstName": self.firstnameTextField.text,
                                                                   @"Email": self.emailTextField.text,
                                                                   @"Company": self.companyTextField.text,
                                                                   @"JobTitle": self.jobtitleTextField.text
                                                                   }
                                                     andListName:LIST_NAME
                                                        callback:^(NSError *error) {
                                                            dispatch_sync(dispatch_get_main_queue(), ^{
                                                                [self performSegueWithIdentifier:@"UpdateList" sender: nil];
                                                            });
                                                        }];
            [task resume];
        }
            break;
        case View: {
            self.lastNameTextField.enabled = YES;
            self.firstnameTextField.enabled = YES;
            self.emailTextField.enabled = YES;
            self.companyTextField.enabled = YES;
            self.jobtitleTextField.enabled = YES;
            self.navigationItem.rightBarButtonItem.title = @"Save";
            self.action = Update;
        }
            
        default:
            break;
    }
}
- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
