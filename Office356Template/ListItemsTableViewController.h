//
//  ListItemsTableViewController.h
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListClient+ListItemClient.h"
#import "Office365ClientFetcher.h"

@interface ListItemsTableViewController : UITableViewController

@property (strong, nonatomic) Office365ClientFetcher *baseController;
@property (nonatomic, strong) NSMutableArray *listArray;

@end
