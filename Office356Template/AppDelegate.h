//
//  AppDelegate.h
//  Office356Template
//
//  Created by P3OPLE 4U on 5/20/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

