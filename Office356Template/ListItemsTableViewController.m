//
//  ListItemsTableViewController.m
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "ListItemsTableViewController.h"
#import "MSDiscoveryServiceInfoCollectionFetcher.h"
#import "Office365ClientFetcher+SharepointClient.h"
#import "CreateUpdateItemViewController.h"

#define LIST_NAME @"MckeinContacts"

NSString * const CELL_REUSE_IDENTIFIER = @"ListItemCell";

@interface ListItemsTableViewController ()

@property (strong, nonatomic) ListClient* listClient;
@property (strong, nonatomic) UIRefreshControl* refreshControl;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation ListItemsTableViewController

- (Office365ClientFetcher *)baseController
{
    if (!_baseController) {
        _baseController = [[Office365ClientFetcher alloc] init];
    }
    
    return _baseController;
}

-(NSMutableArray *)listArray {
    if (!_listArray) {
        _listArray = [[NSMutableArray alloc] init];
    }
    return _listArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

    [self setupView];
    
    [self getListItems];
}

#pragma mark - private methods

//Setup methods
-(void)setupView{
    self.title = @"Contacts";
    
    //Tableview customizations
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(getListItems) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    //Activity Indicator
    self.activityIndicator.center = self.tableView.center;
    [self.view addSubview:self.activityIndicator];
    [self startloading];
}

//Show methods
- (IBAction)showItem:(id)sender {
    [self performSegueWithIdentifier:@"CreateItem" sender:nil];
}

//Get methods
-(void)getListItems {
    
    __weak ListItemsTableViewController *weakSelf = self;
    
    if (self.listClient) {
        
        NSURLSessionTask* task = [self.listClient getListItems:LIST_NAME callback:^(NSMutableArray *listItems, NSError *error) {
            if ([listItems count] > 0) {
                for (ListItem *list in listItems) {
                    NSLog(@"item: %@", list.getRawResults);
                    
                }
                [weakSelf.listArray removeAllObjects];
                [weakSelf.listArray addObjectsFromArray:listItems];
                
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [weakSelf.tableView reloadData];
                });
                
            }
            dispatch_sync(dispatch_get_main_queue(), ^{
                [self stoploading];
                [self.refreshControl endRefreshing];
            });
        }];
        [task resume];
    } else {
        [self.baseController fetchListClient:^(ListClient *listClient) {
            
            if (listClient) {
                weakSelf.listClient = listClient;
                
                NSURLSessionTask* task = [listClient getListItems:LIST_NAME callback:^(NSMutableArray *listItems, NSError *error) {
                    if ([listItems count] > 0) {
                        for (ListItem *list in listItems) {
                            NSLog(@"item: %@", list.getRawResults);
                            
                        }
                        [weakSelf.listArray removeAllObjects];
                        [weakSelf.listArray addObjectsFromArray:listItems];
                        
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [weakSelf.tableView reloadData];
                        });
                        
                    }
                    dispatch_sync(dispatch_get_main_queue(), ^{
                        [self stoploading];
                        [self.refreshControl endRefreshing];
                    });
                }];
                [task resume];
                
            } else {
                //There was no list client detected
            }
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.listArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    
    // Configure the cell...
    ListItem *contactItem = (ListItem *)self.listArray[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", contactItem.getRawResults[@"FirstName"], contactItem.getRawResults[@"Title"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //Delete records
        __weak ListItemsTableViewController *weakSelf = self;
        [self startloading];
        NSURLSessionTask* task = [self.listClient deleteItem:(ListItem *)self.listArray[indexPath.row] andListName:LIST_NAME callback:^(NSError *error) {
            if (error) {
                //delete not successful
            } else {
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [self stoploading];
                    [weakSelf.listArray removeObjectAtIndex:indexPath.row];
                    [weakSelf.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                });

            }
        }];
        
        [task resume];
        
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForSwipeAccessoryButtonForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return NSLocalizedString(@"Edit", nil);
}

-(void)tableView:(UITableView *)tableView swipeAccessoryButtonPushedForRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"UpdateItem" sender:(ListItem *)self.listArray[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"ViewItem" sender:(ListItem *)self.listArray[indexPath.row]];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation beforetitnavigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"CreateItem"]) {
        CreateUpdateItemViewController *controller = [(UINavigationController *)segue.destinationViewController viewControllers][0];
        controller.listClient = self.listClient;
        controller.action = Create;
    } else if ([segue.identifier isEqualToString:@"UpdateItem"]) {
        CreateUpdateItemViewController *controller = [(UINavigationController *)segue.destinationViewController viewControllers][0];
        controller.listClient = self.listClient;
        controller.action = Update;
        controller.listItem = (ListItem *) sender;
    } else if ([segue.identifier isEqualToString:@"ViewItem"]) {
        CreateUpdateItemViewController *controller = [(UINavigationController *)segue.destinationViewController viewControllers][0];
        controller.listClient = self.listClient;
        controller.action = View;
        controller.listItem = (ListItem *) sender;
    }
}

- (IBAction)updateList:(UIStoryboardSegue*) segue {
    [self startloading];
    [self getListItems];
}

-(void)startloading{
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
}

-(void)stoploading{
    self.activityIndicator.hidden = YES;
    [self.activityIndicator stopAnimating];
}


@end
