//
//  CreateUpdateItemViewController.h
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListClient+ListItemClient.h"
#import "ListItem.h"

typedef enum : NSUInteger {
   Create,
   Update,
   View,
} Action;

@interface CreateUpdateItemViewController : UIViewController

@property (nonatomic, weak) ListClient *listClient;
@property (weak, nonatomic) IBOutlet UITextField *firstnameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *companyTextField;
@property (weak, nonatomic) IBOutlet UITextField *jobtitleTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *actionButton;

@property (assign, nonatomic) Action action;
@property (nonatomic, weak) ListItem *listItem;

@end
