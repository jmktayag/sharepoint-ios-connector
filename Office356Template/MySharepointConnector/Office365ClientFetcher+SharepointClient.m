//
//  Office365ClientFetcher+SharepointClient.m
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "Office365ClientFetcher+SharepointClient.h"
#import "AuthenticationManager.h"
#import "OAuthentication.h"

static NSString * const SHAREPOINT_URI  = @"https://p34demo.sharepoint.com";

@implementation Office365ClientFetcher (SharepointClientFetcher)

- (void)fetchListClient:(void (^)(ListClient *listClient))callback {
    
    AuthenticationManager *authenticationManager = [AuthenticationManager sharedInstance];
    
    [authenticationManager acquireAuthTokenWithResourceId:SHAREPOINT_URI
                                        completionHandler:^(BOOL authenticated) {
                                            if (authenticated) {
                                                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                                
                                                NSString *globalToken = [userDefaults valueForKey:@"AccessToken"];
                                                OAuthentication *credentials = [[OAuthentication alloc] initWith:globalToken];
                                                callback([[ListClient alloc] initWithUrl:SHAREPOINT_URI credentials:credentials]);
                                                
                                            }
                                            else {
                                                
                                            }
                                            
                                        }];
}

@end
