//
//  Office365ClientFetcher+SharepointClientFetcher.h
//  Office356Template
//
//  Created by P3OPLE 4U on 6/10/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "Office365ClientFetcher.h"
#import "ListClient+ListItemClient.h"

@interface Office365ClientFetcher (SharepointClientFetcher)

- (void)fetchListClient:(void (^)(ListClient *listClient))callback;

@end
