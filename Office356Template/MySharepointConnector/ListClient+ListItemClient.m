//
//  ListClient+ListItem.m
//  Office356Template
//
//  Created by P3OPLE 4U on 6/16/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "ListClient+ListItemClient.h"
#import "NSString+NSStringExtensions.h"
#import "HttpConnection.h"
#import "Constants.h"

@implementation ListClient (ListItemClient)

const NSString *sharepointApiUrl = @"/_api/lists";

-(NSURLSessionDataTask *)createItemWithData:(NSDictionary *)data WithListName:(NSString *)name callback:(void (^)(ListItem *, NSError *))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items", self.Url , sharepointApiUrl, [name urlencode]];
    
    //Build item here to get raw results
    ListItem *createListItem = [[ListItem alloc] initWithDictionary:data];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:createListItem.getRawResults options:NSJSONWritingPrettyPrinted error:&error];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url
                                                                   bodyArray: jsonData];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Post;
    
    return [connection execute:method callback:^(NSData  *data, NSURLResponse *reponse, NSError *error) {
        
        ListItem *list;
        
        if(error == nil){
            list = [[ListItem alloc] initWithJson:data];
        }
        
        callback(list, error);
    }];
}

//Update
- (NSURLSessionDataTask *)updateItem:(ListItem *)item withData:(NSDictionary *)data andListName:(NSString *)name callback:(void (^)(NSError *error))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items(%@)", self.Url , sharepointApiUrl, [name urlencode], item.Id];
    
    NSError *error;
    
    //apply updating here
    [item createFromJson:data];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:item.getRawResults options:NSJSONWritingPrettyPrinted error:&error];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url
                                                                   bodyArray: jsonData];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Post;
    [connection.request addValue:@"MERGE" forHTTPHeaderField:@"X-HTTP-Method"];
    
    return [connection execute:method callback:^(NSData  *data, NSURLResponse *reponse, NSError *error) {
        callback(error);
    }];
}

//Updating of list item by Id
- (NSURLSessionDataTask *)updateItemWithId:(NSString *)itemId withData:(NSDictionary *)data andListName:(NSString *)name callback:(void (^)(NSError *error))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items(%@)", self.Url , sharepointApiUrl, [name urlencode], itemId];
    
    
    NSError *error;
    
    //apply updating here
    ListItem *updateItem = [[ListItem alloc] initWithDictionary:data];
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:updateItem.getRawResults options:NSJSONWritingPrettyPrinted error:&error];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url
                                                                   bodyArray: jsonData];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Post;
    [connection.request addValue:@"MERGE" forHTTPHeaderField:@"X-HTTP-Method"];
    
    return [connection execute:method callback:^(NSData  *data, NSURLResponse *reponse, NSError *error) {
        callback(error);
    }];
    
}

//Delete
- (NSURLSessionDataTask *)deleteItem:(ListItem *)item andListName:(NSString *)name callback:(void (^)(NSError *error))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items(%@)", self.Url , sharepointApiUrl, [name urlencode], item.Id];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url];
    [connection.request addValue:@"DELETE" forHTTPHeaderField:@"X-HTTP-Method"];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Post;
    
    return [connection execute:method callback:^(NSData  *data, NSURLResponse *reponse, NSError *error) {
        
        callback(error);
    }];
    
}

//Delete by Item Id
- (NSURLSessionDataTask *)deleteItemWithId:(NSString *)itemId andListName:(NSString *)name callback:(void (^)(NSError *error))callback {
    
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items(%@)", self.Url , sharepointApiUrl, [name urlencode], itemId];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url];
    [connection.request addValue:@"DELETE" forHTTPHeaderField:@"X-HTTP-Method"];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Post;
    
    return [connection execute:method callback:^(NSData  *data, NSURLResponse *reponse, NSError *error) {
        
        callback(error);
    }];
    
}

//Fetch of specific list item
- (NSURLSessionDataTask *)fetchItemWithId:(NSString *)itemId andListName:(NSString *)name callback:(void (^)(ListItem *, NSError *error))callback {
    NSString *url = [NSString stringWithFormat:@"%@%@/GetByTitle('%@')/items(%@)", self.Url , sharepointApiUrl, [name urlencode], itemId];
    
    HttpConnection *connection = [[HttpConnection alloc] initWithCredentials:self.Credential
                                                                         url:url];
    
    NSString *method = (NSString*)[[Constants alloc] init].Method_Get;
    
    return [connection execute:method callback:^(NSData *data, NSURLResponse *reponse, NSError *error) {
        
        NSLog(@"**DATA RESPONSE: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        
        ListItem *list;
        
        if(error == nil){
            list = [[ListItem alloc] initWithJson:data];
        }
        
        callback(list, error);
    }];
}


@end
