//
//  ListClient+ListItemClient.h
//  Office356Template
//
//  Created by P3OPLE 4U on 6/16/15.
//  Copyright (c) 2015 P3OPLE 4U. All rights reserved.
//

#import "ListClient.h"
#import "ListItem.h"

@interface ListClient (ListItemClient)

//Creation of list item
- (NSURLSessionDataTask *)createItemWithData:(NSDictionary *)data WithListName:(NSString *)name callback:(void (^)(ListItem *item, NSError *error))callback;

//Updating of list item
- (NSURLSessionDataTask *)updateItem:(ListItem *)item withData:(NSDictionary *)data andListName:(NSString *)name callback:(void (^)(NSError *error))callback;

//Updating of list item by Id
- (NSURLSessionDataTask *)updateItemWithId:(NSString *)itemId withData:(NSDictionary *)data andListName:(NSString *)name callback:(void (^)(NSError *error))callback;

//Deleting of list item
- (NSURLSessionDataTask *)deleteItem:(ListItem *)item andListName:(NSString *)name callback:(void (^)(NSError *error))callback;

//Delete item with id
- (NSURLSessionDataTask *)deleteItemWithId:(NSString *)itemId andListName:(NSString *)name callback:(void (^)(NSError *error))callback;

//Fetch of specific list item
- (NSURLSessionDataTask *)fetchItemWithId:(NSString *)id andListName:(NSString *)name callback:(void (^)(ListItem *item,NSError *error))callback;

@end
