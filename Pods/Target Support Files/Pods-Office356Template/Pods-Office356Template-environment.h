
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ADALiOS
#define COCOAPODS_POD_AVAILABLE_ADALiOS
#define COCOAPODS_VERSION_MAJOR_ADALiOS 1
#define COCOAPODS_VERSION_MINOR_ADALiOS 2
#define COCOAPODS_VERSION_PATCH_ADALiOS 2

// Office365/Discovery
#define COCOAPODS_POD_AVAILABLE_Office365_Discovery
#define COCOAPODS_VERSION_MAJOR_Office365_Discovery 0
#define COCOAPODS_VERSION_MINOR_Office365_Discovery 9
#define COCOAPODS_VERSION_PATCH_Office365_Discovery 1

// Office365/Lists
#define COCOAPODS_POD_AVAILABLE_Office365_Lists
#define COCOAPODS_VERSION_MAJOR_Office365_Lists 0
#define COCOAPODS_VERSION_MINOR_Office365_Lists 9
#define COCOAPODS_VERSION_PATCH_Office365_Lists 1

// Office365/OData
#define COCOAPODS_POD_AVAILABLE_Office365_OData
#define COCOAPODS_VERSION_MAJOR_Office365_OData 0
#define COCOAPODS_VERSION_MINOR_Office365_OData 9
#define COCOAPODS_VERSION_PATCH_Office365_OData 1

// Office365/Outlook
#define COCOAPODS_POD_AVAILABLE_Office365_Outlook
#define COCOAPODS_VERSION_MAJOR_Office365_Outlook 0
#define COCOAPODS_VERSION_MINOR_Office365_Outlook 9
#define COCOAPODS_VERSION_PATCH_Office365_Outlook 1

